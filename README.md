# How to build, install and run a CiviCRM demo instance on Drupal10 on Debian 11.x 

[[_TOC_]]

## Notes

The CiviCRM docs state [that any CiviCRM version above 5 is only compatible with php 8.1](https://docs.civicrm.org/installation/en/latest/general/requirements/)

---

After having build the `buildkit` tools, one cannot run `drush` from the `buildkit/bin` folder directly, due to the following error:

`.../buildkit/bin/extern/drush8.phar not found`

One must go one folder up and run `./bin/drush` from there, as the missing `extern` folder resides in `buildkit/extern` 

---

Once the demo bundle is running locally and one is accessing Drupal via the web-browser, then the Drupal theme is broken as no css files have been generated. One can fix this issue by [disabling css and js file aggregation](https://stackoverflow.com/questions/77497744/drupal-10-sites-default-files-css-and-js-folders-files-missing) as [shown later on below](#configure-drupal).

## Getting Started

Prepare a work envíronment

```bash
# make directories for building civicrm tools, bu bundles and for making backups of config files
mkdir -p ~/build ~/backup/php-config
```
---
Needed packages and versions

_(See [civicrm requirements for a list of needed packages](https://docs.civicrm.org/dev/en/latest/basics/requirements/))_

- apache version 2, ie. `apache2` version 2.4 (Debian 11)
- php version 8, ie. `php8.1` (not available on Debian 11)
- mariadb version 10, ie. `mariadb-server-10.5` version 10.5 (Debian 11)
- npm version 6, ie. `npm` version 7.5 (Debian 11)
- nodejs version 8, ie. `nodejs` version 12.22 (Debian 11)
- `git` _(recent version on Debian 11)_
---
Figure out which needed packages have been installed already

```bash
# find out if apache and php have been installed already and in which version
apt-cache policy apache2
apt-cache policy php*-common
```

```bash
# Dependencies that are needed to be installed
apt-cache policy git
apt-cache policy mariadb-server-*
apt-cache policy nodejs
apt-cache policy npm
```
---
Finally, the CiviCRM demo bundle will include (this will be specified in one of the next steps)

- drupal version 10
- civicrm version 5.67.1 _(latest stable version as of November 2023)_

## Install php8 on Debian 11.x

In order to run `Drupal10` with the latest `CiviCRM`, `php8.1` is needed but `Debian 11` does only provide `php7.4`.

_(Which php version to be used for what Drupal version can be verified at the [Drupal PHP-requirements page](https://www.drupal.org/docs/getting-started/system-requirements/php-requirements).)_

---

[Lets see how to install php8.1 with Apache on Debian 11 Linux](https://www.cyberciti.biz/faq/install-php-8-2-with-apache-on-debian-11-linux/):

```bash
# Remove existing php7.x version, if installed at all

# Backup PHP config files if needed later
cp -avr /etc/php/* ~/backup/php-config/

# Remove all installed php-packages
old_pkgs="$(dpkg --list | grep -E '^ii.*php' | awk '/^ii/{ print $2}')"
apt --purge remove ${old_pkgs}
```

```bash
# Add php8 repository as apt-source

sudo curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg

sudo echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list

# fetch packages in new repository
sudo apt update
```

```bash
# Install php8.1 plus necessary modules

sudo apt install php8.1
sudo apt install \
  php8.1-xml \
  php8.1-curl \
  php8.1-mysql \
  php8.1-mbstring \
  php8.1-bcmath \
  php8.1-gd \
  php8.1-zip \
  php8.1-imap \
  php8.1-intl
```

## Install php development tools

Optionally, install a php-debugger and the unit-test framework in order to write CiviCRM add-ons.

On Debian 11, the `phpunit` package is of version `9.5`. [This version should be compatible with php8](https://phpunit.de/supported-versions.html).

```bash
# the php debugger
sudo apt install php8.1-xdebug

# the php unittest framework
sudo apt install phpunit
```

_(alternatively one can also [use the DePHPugger debugger](https://hackernoon.com/how-debug-php-applications-with-dephpugger-98cc234d917c)) as well._

## Verify php version

```bash
# verify php version
php -v

> PHP 8.1.26 (cli) (built: Nov 24 2023 13:12:14) (NTS)
> Copyright (c) The PHP Group
> Zend Engine v4.1.26, Copyright (c) Zend Technologies
>     with Zend OPcache v8.1.26, Copyright (c), by Zend Technologies
>     with Xdebug v3.2.1, Copyright (c) 2002-2023, by Derick Rethans
```

```bash
phpunit --version

> PHPUnit 9.5.2 by Sebastian Bergmann and contributors.
```

## Install other dependencies

```bash
# install git and nodejs
sudo apt install git nodejs

# install apache 2.4 and corresponding libs
sudo apt install apache2 libapache2-mod-php8.1

# install mariadb
sudo apt install mariadb-server-10.5 

# optional install a sql admin user interface 
sudo apt install adminer
```

## Install CiviCRM tools

In order to build the demo-bundle, `civibuild` tools need to be installed first, as [described in the generic build instructons for CiviCRM](https://docs.civicrm.org/dev/en/latest/tools/buildkit/generic/).

```bash
cd ~/build

# clone the buildkit repository first
git clone https://github.com/civicrm/civicrm-buildkit.git buildkit

# build the tools
cd buildkit/bin
./civi-download-tools
```

## Configure amp

Now, configure the `amp` tool, that will help to start and stop the demo page instance later on. `amp` has been installed in the previous step.

---

```bash
cd ~/build/buildkit/bin

./amp config
```

Choose the following options offered by interactive `amp` dialogs.

|amp config option|value|comment|
|-----------------|-----|-------|
|db_type|mysql_dsn||

In order to authenticate with dsn-string (as shown in the next table) at a mysql database, one needs to [add a corresponding user to mysql and grant it the proper rights](https://stackoverflow.com/questions/5016505/mysql-grant-all-privileges-on-database/15707789#15707789) first. 

Open a second terminal:

```bash
# enter the mysql prompt
sudo mysql

# write into mysql prompt
GRANT ALL ON *.* to 'sqldemo'@'localhost' IDENTIFIED BY '1234' WITH GRANT OPTION; 
```

Now add the corresponding dns-string to the next interactive option offered by `amp`:

|amp config option|value|comment|
|-----------------|-----|-------|
|mysql_dns|mysql://sqldemo:1234@localhost:3306|sqldemo:1234 is the user and password for sql authentication|

Then continue with the remaining options:

|amp config option|value|comment|
|-----------------|-----|-------|
|perm_type|linuxAcl|needs package `acl`|
|perm_user|www-data||
|host_type|file||
|httpd_type|apache24||
|httpd_visibility|local||
|httpd_restart_command|sudo /usr/sbin/apache2ctl graceful|apache2ctl is installed alongside apache2|

All configuration will be written to the following file

- `~/.amp/services.yml`

---

Test if amp has been configured correctly

```bash
./amp test

# output looks like this

> Create test application
> Create data directory: /home/demo/.amp/canary/data/BUSN6OrxNKVdyDEFJi7WxQzYT2llNb6A
> [sudo] Passwort für demo: 
> AMP_URL='http://localhost:7979'
> AMP_ROOT='/home/demo/.amp/canary'
> AMP_DB_DSN='mysql://canary_85t69:pGuR64TwfKBYEUIy@127.0.0.1:3306/canary_stztq?new_link=true'
> AMP_DB_USER='canary_85t69'
> AMP_DB_PASS='pGuR64TwfKBYEUIy'
> AMP_DB_HOST='127.0.0.1'
> AMP_DB_PORT='3306'
> AMP_DB_NAME='canary_stztq'
> AMP_DB_ARGS='--defaults-file='\''/home/demo/.amp/my.cnf.d/my.cnf-a2b6cabf3f3db8f58b9d382a60683921'\'' canary_stztq'
> 
> Connect to test application
> Expect response: "response-code-AxV3kXtYDG"
> Received expected response
```

## Build CiviCRM with Drupal 10 bundle

Now build a bundle with the following components: 

```bash
cd ~/build/civibuild

buildname='demo-drupal-10-civicrm-5-67-1'
drupaltype='drupal10-demo'
civiver='5.67.1'

bin/civibuild create "${buildname}" --type "${drupaltype}" --civi-ver "${civiver}" --url "http://${buildname}.localhost"
```

The generated bundle can be found at:

- ```~/build/buildkit/build/demo-drupal-10-civicrm-5-67-1```

---

Include a directive for `apache2` configuration generated by `civibuild` to the global `apache2` configuration

```bash
sudo echo "IncludeOptional ${HOME}/.amp/apache.d/*.conf" >> /etc/apache2/apache2.conf
```

---

An alternative, that probably saves the next deployment step, is to use the `civibuild install` command.

There are a few things to consider:

- `civibuild install` should not be run as `root` or via `sudo` (or more precise the underlying [`composer` should not be run as `root`](https://getcomposer.org/doc/faqs/how-to-install-untrusted-packages-safely.md))
- hence, a build directory should be created manually in `\var\www\<build-dir>`
- that dir needs `root:www-data` ownership and `775` permissions
- the linux user that builds the demo page in `\var\www` needs to be part of group `www-data`

The next steps would look like this (but have not been tested yet):

```bash
cd /var/www

sudo mkdir demo-drupal-10-civicrm-5-67-1
sudo chown -R root:www-data demo-drupal-10-civicrm-5-67-1
sudo chmod -R 775 demo-drupal-10-civicrm-5-67-1

cd ~/build/civibuild/bin

buildname='demo-drupal-10-civicrm-5-67-1'
drupaltype='drupal10-demo'
civiver='5.67.1'

./civibuild create "${buildname}" --web-root "\var\www\${buildname}" --type "${drupaltype}" --civi-ver "${civiver}" --url "http://${buildname}.localhost"
```

## Deploy demo page on local machine

As it is more difficult to host the generated demo page from its build directory (due to complex permission situation for apache), lets move the demo page to [the proper folder provided by Debian](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-11).

```bash
sudo mv ~/build/buildkit/build/demo-drupal-10-civicrm-5-67-1 /var/www/
sudo mv ~/build/buildkit/build/demo-drupal-10-civicrm-5-67-1.sh /var/www/
sudo mv ~/build/buildkit/app/private/demo-drupal-10-civicrm-5-67-1 /var/www/demo-drupal-10-civicrm-5-67-1-private

# change permission and ownership so that apache2/php can access the site folders
cd /var/www
sudo chown -R root:www-data demo-drupal-10-civicrm-5-67-1*
sudo chmod -R 775 demo-drupal-10-civicrm-5-67-1*

# the folowing folders and files should be visible now
ls -la

> drwxrwxr-x  5 root www-data 4096 24. Nov 16:04 demo-drupal-10-civicrm-5-67-1
> drwxrwxr-x  3 root www-data 4096 24. Nov 17:18 demo-drupal-10-civicrm-5-67-1-private
> -rwxrwxr-x  1 root www-data 2091 24. Nov 16:21 demo-drupal-10-civicrm-5-67-1.sh

```

---

Now, a couple of configuration files need to be adjusted to the path of the new site directory

- `~/.amp/apache.d/demo-drupal-10-civicrm-5-67-1-www.localhost_80.conf`
- `/var/www/demo-drupal-10-civicrm-5-67-1.sh`
- `/var/www/demo-drupal-10-civicrm-5-67-1/web/sites/default/settings.php`
- `/var/www/demo-drupal-10-civicrm-5-67-1/web/sites/default/civicrm-settings.php`

---

In `~/.amp/apache.d/demo-drupal-10-civicrm-5-67-1-www.localhost_80.conf`:

|variable|value|
|--------|-----|
|`DocumentRoot`|`/var/www/demo-drupal-10-civicrm-5-67-1-www/web`|
|`Directory`|`/var/www/demo-drupal-10-civicrm-5-67-1-www/web`|

---

In `/var/www/demo-drupal-10-civicrm-5-67-1.sh`:

|variable|value|
|--------|-----|
|`CMS_ROOT`|`/var/www/demo-drupal-10-civicrm-5-67-1/web`|
|`WEB_ROOT`|`/var/www/demo-drupal-10-civicrm-5-67-1`|

---

In `/var/www/demo-drupal-10-civicrm-5-67-1/web/sites/default/settings.php` set:

|variable|value|
|--------|-----|
|`$civibuild['CMS_ROOT']`|`/var/www/demo-drupal-10-civicrm-5-67-1/web`|
|`$civibuild['WEB_ROOT']`|`/var/www/demo-drupal-10-civicrm-5-67-1`|

---

In `/var/www/demo-drupal-10-civicrm-5-67-1/web/sites/default/civicrm.settings.php`:

|variable|value|
|--------|-----|
|`$civibuild['CMS_ROOT']`|`/var/www/demo-drupal-10-civicrm-5-67-1/web`|
|`$civibuild['WEB_ROOT']`|`/var/www/demo-drupal-10-civicrm-5-67-1`|
|`civibuild['PRIVATE_ROOT']`|`/var/www/demo-drupal-10-civicrm-5-67-1-private```|
|`$civicrm_root`|`/var/www/demo-drupal-10-civicrm-5-67-1/vendor/civicrm/civicrm-core/`|
|`CIVICRM_TEMPLATE_COMPILEDIR`|`/var/www/demo-drupal-10-civicrm-5-67-1-private/default/civicrm/templates_c`|

---

Finally, remove write-permissions for the following files and folders, as Drupal will complain about too wide permissions:

```bash
cd /var/www/demo-drupal-10-civicrm-5-67-1/web/

sudo chmod 755 sites/default
sudo chmod 755 sites/default/settings.php
sudo chmod 755 sites/default/civicrm.settings.php
```

## Configure Drupal

By default, the demo Drupal page offers just a `demo` account with limited admin rights that does not allow to change Drupal configuration, but only allows access to the CiviCRM module. Lets

- create a new user with full admin rights
- changes Drupal configuration in order to fix the broken theme

---

In order to do this we are using the `drush` tool that resides in `~/build/buildkit/bin/drush`.

**Note** `drush` must be run from `~/build/buildkit` as it generates an error otherise [see Notes above](#notes)

**Note** `drush` needs to know the Drupal root dir, in order to change the proper Drupal configuration. The root dir can be passed with the `-r` option.

---

```bash
cd ~/build/buildkit

# fix missing generated theme css and js files
bin/drush -r /var/www/demo-drupal-10-civicrm-5-67-1/web config:set system.performance css.preprocess false
bin/drush -r /var/www/demo-drupal-10-civicrm-5-67-1/web config:set system.performance js.preprocess false

# add a new user
bin/drush -r /var/www/demo-drupal-10-civicrm-5-67-1/web user:create me --mail='me@localhost' --password '12341234' 

# assign admin role to new user
bin/drush -r /var/www/demo-drupal-10-civicrm-5-67-1/web user-add-role administrator me
```

---

[Drupal expects that `trusted_host_patterns`](https://www.drupal.org/docs/getting-started/installing-drupal/trusted-host-settings) are defined in its `settings.php` file. 

Therefore, add the following configuration to  `/var/www/demo-drupal-10-civicrm-5-67-1/web/sites/default/settings.php`

```php
$settings['trusted_host_patterns'] = array(
  '^localhost$',
  '^demo-drupal-10-civicrm-5-67-1-www\.localhost$',
);
```

## Run Drupal and CiviCRM demo bundle

Most likely, `apache2` is running already and is hosting a default page under `http://localhost` address.

Lets stop `apache2` first

```bash
# use apache2ctl
apache2ctl -k status
sudo apache2ctl -k stop
```

---

Now, create a new apache2 instance for the root dir `/var/www/demo-drupal-10-civicrm-5-67-1/web` of the Drupal demo site with the `amp` tool

```bash
cd ~/build/buildkit/bin
./amp create -r /var/www/demo-drupal-10-civicrm-5-67-1/web -f -vvv

# amp output might look like this

> Box Requirements Checker
> ========================
> 
> Using PHP 8.1.26
> PHP is using the following php.ini file:
>   /etc/php/8.1/cli/php.ini
> 
> Checking Box requirements:
>   ✔ The application requires the version ">=7.1" or greater.
> 
> [OK] Your system is ready to run the application.                                        >                                       
> [sudo] Passwort für demo: 
> 
> httpd not running, trying to start
> httpd not running, trying to start
> 
> AMP_URL='http://localhost:7979'
> AMP_ROOT='/var/www/demo-drupal-10-civicrm-5-67-1/web'
> AMP_DB_DSN='mysql://demodrupal_mq28f:4b3KBtwtYQMU4vxg@127.0.0.1:3306/demodrupal10civicrm5671_d0tu8?new_link=true'
> AMP_DB_USER='demodrupal_mq28f'
> AMP_DB_PASS='4b3KBtwtYQMU4vxg'
> AMP_DB_HOST='127.0.0.1'
> AMP_DB_PORT='3306'
> AMP_DB_NAME='demodrupal10civicrm5671_d0tu8'
> AMP_DB_ARGS='--defaults-file='\''/home/demo/.amp/my.cnf.d/my.cnf-324f83c6103234abefe9f73d56831881'\'' demodrupal10civicrm5671_d0tu8'
```

---

`amp` can also show all instances that have been started so far:

```bash
~/build/buildkit/bin amp show

# instances are stored in file
cat ~/.amp/instances.yml
```

---

Access the demo Drupal page via the browser _(uri has been defined in [Build CiviCRM with Drupal 10 bundle](#build-civicrm-with-drupal-10-bundle))_

- `http://demo-drupal-10-civicrm-5-67-1-www.localhost` 

Login as:

- user: `me`
- pwd: `12341234`

Check the status of this instance first

- `http://demo-drupal-10-civicrm-5-67-1-www.localhost/admin/reports/status`

## Initialise CiviCRM addon-skeleton

Initial steps for writing a new CiviCRM add-on

```bash
cd /var/www/demo-drupal-10-civicrm-5-67-1/web/sites/default/files/civicrm/ext

# generates a new folder my-first-plugin with a addon skeleton 
~/build/buildkit/bin/civix generate:module my-first-plugin

# plugin metadata can be adjusted as well
my-first-plugin/info.xml
```

---

The new CiviCRM add-on should show up already under

- `http://demo-drupal-10-civicrm-5-67-1-www.localhost/civicrm/admin/extensions`

# References

Requirements

- https://docs.civicrm.org/dev/en/latest/basics/requirements/
- https://docs.civicrm.org/installation/en/latest/general/requirements/
- https://www.drupal.org/docs/getting-started/system-requirements/php-requirements
- https://phpunit.de/supported-versions.html

How To

- https://www.cyberciti.biz/faq/install-php-8-2-with-apache-on-debian-11-linux/
- https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-11

CiviCRM bundle

- https://docs.civicrm.org/dev/en/latest/tools/buildkit/generic/
- https://docs.civicrm.org/dev/en/latest/tools/civibuild/
- https://docs.civicrm.org/dev/en/latest/testing/
- https://docs.civicrm.org/dev/en/latest/testing/phpunit/

CiviCRM extension

- https://docs.civicrm.org/dev/en/latest/extensions/
- https://docs.civicrm.org/dev/en/latest/hooks/list/

PHP Unit Testing

- https://phpunit.de/getting-started/phpunit-9.html
- https://pguso.medium.com/a-beginners-guide-to-phpunit-writing-and-running-unit-tests-in-php-d0b23b96749f
- https://techviewleo.com/install-and-use-phpunit-on-debian/

PHP Debugging

- https://xdebug.org/docs/step_debug
- https://stackoverflow.com/questions/8114844/starting-xdebug-inside-code
- https://stackoverflow.com/questions/2288612/how-to-trigger-xdebug-profiler-for-a-command-line-php-script

PHP Spell Checking

- https://stackoverflow.com/questions/12152765/php-syntax-checking-with-lint-and-how-to-do-this-on-a-string-not-a-file

Drupal

- https://stackoverflow.com/questions/77497744/drupal-10-sites-default-files-css-and-js-folders-files-missing
- https://www.drupal.org/docs/getting-started/installing-drupal/trusted-host-settings
- https://getcomposer.org/doc/faqs/how-to-install-untrusted-packages-safely.md

Apache

- https://cwiki.apache.org/confluence/display/httpd/13PermissionDenied

# License

[GPL3](https://www.gnu.org/licenses/gpl-3.0.html#license-text)
